from django.urls import include, path
from .views import track, carousel, language

urlpatterns = [
    path('track', track, name="track"),
    path('carousel', carousel, name="carousel"),
    path('language', language, name='language')
]