from django.shortcuts import render

# Create your views here.
def track(request):
    return render(request, "track.html"); 

def carousel(request):
    return render(request, "c_in_m.html");

def language(request):
    return render(request, "language.html");